\documentclass[aip,reprint,nofootinbib,floatfix]{revtex4-1} % for checking your page length

\usepackage{graphicx}

\usepackage[spanish, es-tabla]{babel} % http://minisconlatex.blogspot.com.ar/2013/03/latex-en-espanol.html
\def\spanishoptions{argentina}
\usepackage{babelbib}
\selectbiblanguage{supinas}

\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}

\usepackage[arrowdel]{physics}
\usepackage[separate-uncertainty=true, multi-part-units=single, locale=FR]{siunitx}
\usepackage{isotope} % $\isotope[A][Z]{X}\to\isotope[A-4][Z-2]{Y}+\isotope[4][2]{\alpha}


%% biblatex$
%\usepackage[style=numeric, backend=biber, sorting= none]{biblatex}
% \DefineBibliographyStrings{spanish}{}
% \usepackage{csquotes}
%\addbibresource{Bibliografia.bib}


\begin{document}

\title{Viscosidad en fluidos}

%\author{D. T. W. Buckingham}
%\email[]{dbuckingham@physics.montana.edu}
%\homepage[]{Your web page}
%\thanks{}
%\altaffiliation{}
\affiliation{Laboratorio de Física 1 para estudiantes de la Lic. en Cs. Químicas, Departamento de Física - FCEyN - UBA}

%\date{verano 2019}

\begin{abstract}
\noindent
\textbf{Objetivo:}
En esta experiencia de laboratorio se estudiará el movimiento de caída de una esfera en el seno de un fluido, analizando en particular el comportamiento de la fuerza viscosa.\\ \\
\textbf{Temáticas}: viscosidad, velocidad límite.
\end{abstract}

\maketitle %\maketitle must follow title, authors, abstract and \pacs

\section{Introducción}

Un cuerpo en caída libre en el vacío solo está sometido al efecto de peso.
Su aceleración es constante (\(g\)) y su velocidad aumenta proporcionalmente con el tiempo.
¿Qué diferencia hay cuando el movimiento de caída se da en el seno de un fluido viscoso, ya sea en aire o en un líquido?
En la figura \ref{fig:diagramaCuerpoLibre} se muestra el diagrama de cuerpo libre para un cuerpo que cae en un medio viscoso.
\begin{figure}[ht]
	\centering
	\includegraphics[width=2cm]{./diagramaCuerpoLibre}
	\caption{Diagrama de cuerpo libre de una esfera en el seno de un fluido viscoso.}
	\label{fig:diagramaCuerpoLibre}
\end{figure}
Además de a su propio peso (\(m g\)), el cuerpo es sometido a una fuerza denominada ``empuje'' (\(E\)), de sentido contrario al peso, por el solo hecho de encontrarse sumergido.
Según el principio de Arquímedes, el empuje es igual al peso del líquido desalojado:
\begin{equation}
	E= g \delta_\textrm{líq} V_c,
	\label{eq:empuje}
\end{equation}
siendo \(\delta_\textrm{líq}\) la densidad del líquido y \(V_c\) el volumen del cuerpo sumergido.
Además, si el cuerpo se mueve aparece una fuerza viscosa (\(F_V\)) que se opone al movimiento del cuerpo.
A diferencia de la fuerza de rozamiento dinámico entre dos superficies, esta fuerza viscosa es proporcional a la velocidad y depende también del tamaño y forma del cuerpo.
Para el caso de una esfera de radio \(R\) que en un fluido con coeficiente de viscosidad \(\eta\) le presenta un flujo laminar al avanzar con velocidad \(v\), la Ley de Stokes establece que
\begin{equation}
	F_V= 6 \pi \eta  R v.
	\label{eq:stokes}
\end{equation}

Si planteamos la 2"a Ley de Newton con las fuerzas ejercidas sobre el cuerpo, obtenemos:
\begin{equation}
	m g- E- F_V= m a,
	\label{eq:2da}
\end{equation}
donde \(a\) es la aceleración del cuerpo. 
Mientras \(m g > E+ F_V\) el cuerpo acelerará y aumentará su velocidad.
Al aumentar esta última, aumenta la \(F_V\) reduciéndose la aceleración.
En el límite en que \(m g= E+ F_V\), la aceleración de haría nula y, por lo tanto, la velocidad constante al alcanzar su valor límite (\(v \rightarrow v_\textrm{lím}\)).
Para el caso de una esfera, cuyo volumen \(V= \frac{4}{3} \pi R^3\), puede calcularse \(v_\textrm{lím}\) reemplazando las ecuaciones \ref{eq:empuje} y \ref{eq:stokes} en la \ref{eq:2da},
\begin{equation}
	v_\textrm{lím} = \frac{2 R^2 g (\delta_\textrm{esf}- \delta_\textrm{líq}) }{ 9 \eta },
	\label{eq:vlim}
\end{equation}
donde \(\delta_\textrm{esf}\) es la densidad de la esfera.


\section{Actividades de medición}
En el laboratorio se cuenta con probetas que pueden llenarse con detergente y esferas de acero de distintos tamaños.
Se propone estudiar el movimiento de distintas esferas, determinando la velocidad límite alcanzada y analizando si se alcanza efectivamente o no una velocidad constante.
\begin{itemize}
	\item ¿Cómo puede determinarse la velocidad límite? ¿Cuál sería su error?
	\item ¿Cómo podría determinarse \(\eta\) en este experimento?
	\item ¿Cómo determinar la densidad de las esferas y del líquido?
\end{itemize}
En particular, en este práctico se va a adquirir el video de la trayectoria de la esfera de acero cayendo libremente para luego analizar su movimiento con el programa \emph{Tracker}.


\subsection{Programa \emph{Tracker}}
\emph{Tracker} es un software gratuito creado por \emph{Open Source Physics} que permite analizar videos y modelar los resultados.
Se caracteriza por incluir una herramienta que permite el seguimiento de objetos (posición, velocidad y aceleración).
Asimismo provee filtros espaciales para mejorar la imagen y posee perfiles de línea para el análisis de espectros y de patrones de interferencia.
Está especialmente diseñado para ser utilizado en los laboratorios de física de las universidades.

\subsubsection{Ayuda}
\begin{enumerate}
\item Es importante que el fondo y la iluminación sean homogéneos para evitar el ruido en la medición.
\item En cada video que se hace es necesario tener una magnitud patrón para poder calibrar las distancias recorridas.
\item Una vez abierto el archivo en el \emph{Tracker}, hay que elegir los cuadros de interés del video con la herramienta \verb'ajustes de corte', luego cargar la calibración espacial (utilizando el patrón) con la herramienta \verb'calibración' y finalmente agregar el sistema de referencia.
\item La trayectoria de la esfera se puede determinar de forma manual (cuadro a cuadro marcar posición) o de forma automática.
Esta última resulta más precisa.
Para ello usar \verb'crear > masa puntual'.
Se elige el \emph{key frame} (que es lo que va a tener que seguir el programa), esto se hace en el primer cuadro de la trayectoria
presionando \verb'Shift'+ \verb'Ctrl'+ \emph{click} del mouse.
\end{enumerate}

No dude en consultar la documentación del programa en \url{http://www.cabrillo. edu/~dbrown/tracker/}.



\end{document}
