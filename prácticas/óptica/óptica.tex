\documentclass[aip,reprint,nofootinbib,floatfix]{revtex4-1} % for checking your page length

\usepackage{graphicx}

\usepackage[spanish, es-tabla]{babel} % http://minisconlatex.blogspot.com.ar/2013/03/latex-en-espanol.html
\def\spanishoptions{argentina}
\usepackage{babelbib}
\selectbiblanguage{supinas}

\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}

\usepackage[arrowdel]{physics}
\usepackage[separate-uncertainty=true, multi-part-units=single, locale=FR]{siunitx}
\usepackage{isotope} % $\isotope[A][Z]{X}\to\isotope[A-4][Z-2]{Y}+\isotope[4][2]{\alpha}


%% biblatex$
%\usepackage[style=numeric, backend=biber, sorting= none]{biblatex}
% \DefineBibliographyStrings{spanish}{}
% \usepackage{csquotes}
%\addbibresource{Bibliografia.bib}


\begin{document}

\title{Óptica}

%\author{D. T. W. Buckingham}
%\email[]{dbuckingham@physics.montana.edu}
%\homepage[]{Your web page}
%\thanks{}
%\altaffiliation{}
\affiliation{Laboratorio de Física 1 para estudiantes de la Lic. en Cs. Químicas, Departamento de Física - FCEyN - UBA}

%\date{verano 2019}

\begin{abstract}
\noindent
\textbf{Objetivos:}
Estudiar experimentalmente la reflexión y refracción de la luz.
Determinar el índice de refracción de un material.
Observar el fenómeno de reflexión total interna.

\noindent Estudiar cualitativa y cuantitativamente sistemas ópticos que incorporen lentes.

\textbf{Temáticas}: reflexión, refracción (\emph{ley de Sahl-Snell}), lentes.
\end{abstract}

\maketitle %\maketitle must follow title, authors, abstract and \pacs


\section{Introducción}

\subsection{Refracción}
Cuando un haz de luz incide sobre la superficie que separa dos medios, en los cuales la luz se propaga con diferentes velocidades, parte de la misma se transmite y parte se refleja.
Para un medio cualquiera, el índice de refracción \(n\) se define como \(n=c/v\), donde la velocidad de la luz en el
vacío es \(c\) y en el medio \(v\).
La \emph{ley de Sahl-Snell}\footnote{El persa musulmán Ibn Sahl publicó en Baghdad en el año 984 una descripción matématíca de la refracción equivalente a la que Willebrord Snellius publicó en los paises bajos en 1621.} establece que la relación entre el ángulo incidente \(\theta_1\) y el refractado \(\theta_3\) es
\begin{equation}
	n_1 \sen{\theta_1} = n_3 \sen{\theta_3},
\end{equation}
donde \(n_1\) es el índice correspondiente al medio por donde incide el rayo y \(n_3\) el medio por el cual se transmite el rayo.
Similarmente la ley establece que para el ángulo del rayo reflejado \(\theta_2\) nos queda \(\theta_1 = \theta_2\).
En la figura \ref{fig:rayos} se puede observar el diagrama de los rayos incidente, reflejado y refractado.
\begin{figure}[ht]
	\centering
	\includegraphics[width=0.5\textwidth]{./rayos}
	\caption{Diagrama de rayos.}
	\label{fig:rayos}
\end{figure}

\subsection{Lente}
En una lente delgada se cumple la \emph{ecuación del constructor de lentes}
\begin{equation}
	\frac{n_m}{p}+ \frac{n_m}{q} = \left(n_l - n_m \right) \left( \frac{1}{R_1}- \frac{1}{R_2} \right),
\end{equation}
donde \(p\) es la distancia objeto-lente, \(q\) es la distancia pantalla-lente, \(n_m\) y \(n_l\) los índices de refracción del medio y la lente respectivamente y \(R_1\) y \(R_2\) los radios de las dioptras que componen la lente.
Esta ecuación se puede reescribir en la forma de la \emph{ecuación de Gauss}
\begin{equation}
	\frac{1}{p}+ \frac{1}{q} = \frac{1}{f},
\end{equation}
donde \(f\) es la distancia focal de la lente y no sólo depende de la construcción de la lente, sino también del medio donde está inmersa.


\section{Estudio de la reflexión y refracción}

\subsection{Dependencia con ángulo de incidencia}
Se propone investigar la relación entre el ángulo de reflexión y el ángulo de refracción en función del ángulo incidente, para lo que se dispone de una media caña acrílica (tiene forma de “D”), un puntero láser y alfileres.
\begin{enumerate}
	\item Representar gráficamente los ángulos de reflexión y refracción en función del ángulo incidente. Analice la dependencia y discuta sus conclusiones.
	¿Es la forma más adecuada de analizar las relaciones anteriores?
	\item ¿Qué puede decir a cerca de la validez de la \emph{ley de Sahl-Snell} para el caso que acaba de estudiar experimentalmente?
	\item A partir de sus resultados determine el índice de refracción de la luz del material de acrílico.
	\item ¿Cómo estima los errores en sus mediciones?
\end{enumerate}


\subsection{Reflexión total interna}
Utilizando los mismo elementos de la actividad anterior, diseñe un experimento para poder estimar el ángulo de reflexión total interna \(\theta_c\).
¿Encuentra un ángulo para el cual la luz deja de transmitirse al aire?
\begin{enumerate}
	\item Determine en forma directa el ángulo de crítico para el cual deja de existir el rayo transmitido.
	\item ¿Qué sucede cuando se incide con un ángulo mayor al crítico \(\theta_c\)?
	\item ¿Qué otra forma puede utilizar para hallar dicho ángulo?
	Ayuda: utilizar la \emph{ley de Sahl-Snell}.
	\item A partir del ángulo crítico \(\theta_c\) encuentre el valor del índice de refracción del material de acrílico.
	Comparar con el valor hallado en la actividad anterior.
	Busque algún valor tabulado para algunos acrílicos.
\end{enumerate}


\section{Lentes convergentes y divergentes}

\subsection{Lente convergente: estudio cualitativo}
\begin{enumerate}
	\item Describa cómo varían las características de lo que observa al variar la distancia observador–objeto.
	¿La imagen es más grande, más pequeña o igual que el objeto mismo? ¿La imagen es derecha o invertida?
	¿Varían estas imágenes al variar la distancia observador–lente?
	\item Otra propiedad interesante de las lentes convergentes es que forman imágenes reales, es decir imágenes que pueden proyectarse en una pantalla.
	Para realizar esta observación es conveniente disponer de un objeto bien iluminado, por ejemplo, un árbol o un paisaje.
	También será conveniente que quién mide, con su lente y una pantalla se coloque en un lugar con mucha sombra, pero que le permita ver claramente el objeto iluminado.
	Interponga la lente entre el objeto y la pantalla y varíe la distancia lente–pantalla hasta que aparezca una imagen nítida del objeto.
	La imagen en la pantalla aparecerá invertida.
	Este es el principio de funcionamiento de una cámara fotográfica, en la cual el sensor \emph{CMOS} (o \emph{CCD} en las de uso científico) constituye la ``pantalla'' donde se forma la imagen.
	\item El foco de una lente convergente es el punto sobre el eje óptico a una distancia \(f\) de la lente, donde convergen todos los rayos incidentes paraxiales (paralelos y cercanos al eje) luego de la refracción.
	¿De qué forma puede estimar el valor \(f\)?
	\item Obstruya distintas partes de la lente y observe cómo afecta esto a la imagen.
	Describa sus conclusiones.
	\item ¿Cuál es la diferencia entre una imagen real y una imagen virtual?
	¿Qué tipo de imagen es la que se observa en un espejo plano?
	¿Y en uno cóncavo?
	\item ¿Qué tipo de imagen puede ser proyectada sobre una pantalla: una imagen real o una virtual?
	¿Dónde debe ubicarse el objeto respecto de la lente para obtener una imagen que pueda observarse sobre una pantalla?
\end{enumerate}


\subsection{Lente convergente: estudio cuantitativo}
En la figura \ref{fig:convergente} se propone un montaje experimental.
\begin{figure}[ht]
	\centering
	\includegraphics[width=0.5\textwidth]{./convergente}
	\caption{Esquema del montaje para lentes convergentes.}
	\label{fig:convergente}
\end{figure}

\begin{enumerate}
	\item Para diversas distancias objeto–pantalla, encuentre todas las imágenes que pueda variando la posición de la lente.
	¿Para cuántas posiciones de la lente ve imágenes nítidas en la pantalla?
	Cada vez que observe imágenes nítidas, mida las distancias \(p\), \(q\) y los tamaños y orientaciones del objeto y su imagen.
	\item Represente \(q\) en función de \(p\) y también \(q^{-1}\) en función de \(p^{-1}\).
	¿Qué relación encuentra entre \(q\) y \(p\)?
	¿Puede describirse por medio de la \emph{ecuación de Gauss}?
	\item Estime el valor de la distancia \(f\).
	¿Cómo puede estimar las incertidumbres de \(p\) y \(q\)?
	\item Se define el aumento lateral \(m\) como el cociente entre el tamaño de la imagen y el tamaño del objeto.
	Determine experimentalmente el aumento de la imagen que resulta para distintas posiciones relativas entre objeto y lente.
	Compare el resultado de sus mediciones con las predicciones de la óptica geométrica.
	Represente gráficamente \(m\) y el cociente \(q/p\)en función de \(p\) en un mismo gráfico y discuta sus resultados.
	\item Otra propiedad interesante de las lentes convergentes es que sólo forman imagen de un objeto sobre una pantalla cuando la distancia objeto–pantalla \(D = p + q\) cumple la condición \(D > 4 f\).
	Investigue experimentalmente la validez de esta afirmación usando un banco óptico y una lente de distancia focal \(f\) conocida.
	\item Usando la ecuación de Gauss es posible demostrar que si \(D > 4 f\) la lente no forma imagen.
	Demuestre que no es posible la formación de la imagen si \(D < 4 f\).
	Analice el caso particular de \(D = 4 f\).
	¿Dónde se forma la imagen y con qué aumento en este caso?
\end{enumerate}


\subsection{Lente divergente}
En la figura \ref{fig:divergente} se propone otro montaje experimental.
\begin{figure}[ht]
	\centering
	\includegraphics[width=0.5\textwidth]{./divergente}
	\caption{Esquema del montaje para lentes divergentes.}
	\label{fig:divergente}
\end{figure}

\begin{enumerate}
	\item Estas lentes tiene la característica de ser más delgadas en el centro que en la periferia y dan imágenes virtuales de objetos reales (cualquiera sea la posición de éstos), por tal razón no es posible utilizar el mismo método que se usa para lentes convergentes para determinar su distancia focal.
	Demuestre esta afirmación a partir de la ecuación de Gauss.
	\item Un método utilizado para determinar la distancia focal de una lente divergente consiste en medir las distancias objeto-imagen como en el caso de las lentes convergente, como se ve en la figura \ref{fig:divergente}
	Como para determinar \(q\) es necesario que la imagen sea real, para poder verla sobre una pantalla se utiliza, como objeto virtual, la imagen dada por una lente convergente.
	Recoja en la pantalla la imagen del objeto formada por la lente convergente sola.
	¿Cuál es la mínima distancia a la que debe de colocar el objeto de la lente convergente para que se forme una imagen real de esta lente (objeto virtual para la segunda lente)?
	¿Por qué?
	Intercale la lente divergente entre la primera lente (convergente) y la imagen real de la misma.
	Determine la posición del objeto virtual, la segunda lente (divergente) y la posición de la imagen resultante de las dos lentes combinadas con sus respectivos errores.
	Realice hipótesis razonables que le permitan acotar o estimar dichos errores.
\end{enumerate}


\end{document}
